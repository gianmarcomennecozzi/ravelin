package main

import (
	"fmt"
	"log"
	"net/http"
	app "ravelin/server"
)

const PORT = 8080

func main() {
	s := app.NewServer()
	if err := http.ListenAndServe(fmt.Sprintf(":%d", PORT), s.Handler()); err != nil {
		log.Fatalf("Serving error: %s", err)
	}
}
