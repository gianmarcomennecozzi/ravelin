FROM golang:1.15-alpine AS builder
WORKDIR /app
COPY go.mod .
RUN go mod download
COPY ./client ./client
COPY ./server ./server
COPY main.go main.go
RUN CGO_ENABLED=0 GOOS=linux go build -o ravelin .

FROM scratch
WORKDIR /app
COPY ./client ./client
COPY --from=builder /app/ravelin /app/ravelin
ENTRYPOINT ["/app/ravelin"]