package server

import (
	"crypto/rand"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"sync"
)

const (
	MAX_BYTES = 256
	SESSION   = "server-session"
)

type Data struct {
	WebsiteUrl         string
	SessionId          string
	ResizeFrom         Dimension
	ResizeTo           Dimension
	CopyAndPaste       map[string]bool // map[fieldId]true
	FormCompletionTime int             // Seconds
}

type Dimension struct {
	Width  string
	Height string
}

type Server struct {
	store map[string]*Data
	m     sync.RWMutex
}

func NewServer() *Server {
	return &Server{
		store: make(map[string]*Data),
	}
}

//todo manage the data in a better way, maybe create LRU cache
func (s *Server) Handler() http.Handler {
	m := http.NewServeMux()
	m.HandleFunc("/", s.handleIndexPage())
	m.HandleFunc("/resize", POSTEndpoint(s.handleResize()))
	m.HandleFunc("/copy-paste", POSTEndpoint(s.handleCopyPaste()))
	m.HandleFunc("/time-taken", POSTEndpoint(s.handleTimeTaken()))
	return m
}

func (s *Server) handleIndexPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			http.Error(w, "Page not found!", http.StatusNotFound)
			return
		}

		tmpl, err := template.ParseFiles("client/index.html")
		if err != nil {
			log.Printf("Error parsing the html file: %s", err.Error())
			http.Error(w, "Error parsing the html file", http.StatusInternalServerError)
			return
		}

		_, err = r.Cookie(SESSION)
		if err != nil { //means the client is new
			sessionID := s.createSessionID(r)
			http.SetCookie(w, &http.Cookie{
				Name:  SESSION,
				Value: sessionID,
				Path:  "/",
			})
		}

		if err := tmpl.Execute(w, nil); err != nil {
			log.Printf("Error execute template: %s", err.Error())
			http.Error(w, "Error execute template", http.StatusInternalServerError)
		}
	}
}

func (s *Server) handleResize() http.HandlerFunc {

	type resizeMsg struct {
		FWidth  string `json:"from_width"`
		FHeight string `json:"from_height"`
		TWidth  string `json:"to_width"`
		THeight string `json:"to_height"`
	}

	return func(w http.ResponseWriter, r *http.Request) {

		s.m.Lock()
		defer s.m.Unlock()

		data, err := s.getDataFromRequest(r)
		if err != nil {
			log.Printf("Error [resize] get data from request: %s", err.Error())
			http.Error(w, "error", http.StatusInternalServerError)
			return
		}

		var msg resizeMsg
		if err := readJson(w, r, &msg, MAX_BYTES); err != nil {
			log.Printf("Error [resize] read json from request: %s", err.Error())
			http.Error(w, "error", http.StatusInternalServerError)
			return
		}

		// I am assuming the resize occurs just once
		//todo manage this assumption in the client as well
		if data.ResizeFrom.Height == "" {
			data.ResizeFrom.Height = msg.FHeight
			data.ResizeFrom.Width = msg.FWidth
			data.ResizeTo.Height = msg.THeight
			data.ResizeTo.Width = msg.TWidth
			printData(data)
		}
	}
}
func (s *Server) handleTimeTaken() http.HandlerFunc {

	type timeTakenMsg struct {
		Time int `json:"time"`
	}

	return func(w http.ResponseWriter, r *http.Request) {

		s.m.Lock()
		defer s.m.Unlock()

		data, err := s.getDataFromRequest(r)
		if err != nil {
			log.Printf("Error [time-taken] get data from request: %s", err.Error())
			http.Error(w, "error", http.StatusInternalServerError)
			return
		}

		var msg timeTakenMsg
		if err := readJson(w, r, &msg, MAX_BYTES); err != nil {
			log.Printf("Error [time-taken] read json from request: %s", err.Error())
			http.Error(w, "error", http.StatusInternalServerError)
			return
		}

		data.FormCompletionTime = msg.Time
		printData(data)
	}
}

func (s *Server) handleCopyPaste() http.HandlerFunc {

	type copyPasteMsg struct {
		Field string `json:"field"`
	}

	return func(w http.ResponseWriter, r *http.Request) {

		s.m.Lock()
		defer s.m.Unlock()

		data, err := s.getDataFromRequest(r)
		if err != nil {
			log.Printf("Error [copy-paste] get data from request: %s", err.Error())
			http.Error(w, "error", http.StatusInternalServerError)
			return
		}

		var msg copyPasteMsg
		if err := readJson(w, r, &msg, MAX_BYTES); err != nil {
			log.Printf("Error [copy-paste] read json from request: %s", err.Error())
			http.Error(w, "error", http.StatusInternalServerError)
			return
		}

		_, ok := data.CopyAndPaste[msg.Field]
		if !ok {
			data.CopyAndPaste[msg.Field] = true
		}

		printData(data)
	}
}

//Get Data based on cookie value
func (s *Server) getDataFromRequest(r *http.Request) (*Data, error) {
	c, err := r.Cookie(SESSION)
	if err != nil {
		return nil, err
	}
	data, ok := s.store[c.Value]
	if !ok {
		return nil, fmt.Errorf("cookie not found")
	}
	return data, nil
}

//Create a unique session for the client
func (s *Server) createSessionID(r *http.Request) string {
	s.m.Lock()
	defer s.m.Unlock()

	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		log.Printf("Error create session id: %s", err)
		return ""
	}

	sessionID := fmt.Sprintf("%X-%X-%X-%X-%X", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
	_, ok := s.store[sessionID] //get a new id if the previous one already exists
	if ok {
		return s.createSessionID(r)
	}

	data := &Data{
		WebsiteUrl:   r.Host,
		SessionId:    sessionID,
		CopyAndPaste: make(map[string]bool),
	}

	s.store[sessionID] = data

	printData(data)
	return sessionID
}
