package server

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestPOSTEndpoint(t *testing.T) {
	os.Stdout = nil
	next := func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}

	handler := http.HandlerFunc(POSTEndpoint(next))

	tl := []struct {
		name        string
		method      string
		expectedRes int
	}{
		{name: "GET Request", method: http.MethodGet, expectedRes: http.StatusNotFound},
		{name: "POST Request", method: http.MethodPost, expectedRes: http.StatusOK},
	}

	for _, tc := range tl {
		t.Run(tc.name, func(t *testing.T) {
			resp := httptest.NewRecorder()
			req, err := http.NewRequest(tc.method, "/", nil)
			if err != nil {
				t.Fatal(err)
			}

			handler.ServeHTTP(resp, req)

			//check if the response code is the same as te expected one
			if resp.Code != tc.expectedRes {
				t.Fatalf("Error! Expecting %d, got %d", tc.expectedRes, resp.Code)
			}
		})
	}
}

func TestReadJson(t *testing.T) {
	os.Stdout = nil
	// Test with a json pass in the body request
	// It can not throw an error
	testJSON := []byte(`{"test":"whatever"}`)
	req1, err := http.NewRequest(http.MethodPost, "/", bytes.NewBuffer(testJSON))
	if err != nil {
		t.Fatal(err)
	}

	type normalCaseMsg struct {
		Test string `json:"test"`
	}

	var test1 normalCaseMsg
	err = readJson(nil, req1, &test1, 256)
	if err != nil {
		t.Fatalf("Error reading JSON: %v", err)
	}

	// Test with a string pass in the body request
	// It has to throw an error
	testString := []byte("whatever")
	req2, err := http.NewRequest(http.MethodPost, "/", bytes.NewBuffer(testString))
	if err != nil {
		t.Fatal(err)
	}

	var test2 normalCaseMsg
	err = readJson(nil, req2, &test2, 256)
	if err == nil {
		t.Fatalf("Error! string should not be accepted")
	}
}
