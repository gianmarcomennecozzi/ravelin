package server

import (
	"encoding/json"
	"fmt"
	"net/http"
)

//Read json from request
func readJson(w http.ResponseWriter, r *http.Request, i interface{}, bytes int64) error {
	r.Body = http.MaxBytesReader(w, r.Body, bytes)
	defer r.Body.Close()
	if err := json.NewDecoder(r.Body).Decode(i); err != nil {
		return err
	}
	return nil
}

//Check if the request is a POST request, if not return 404
func POSTEndpoint(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			http.NotFound(w, r)
			return
		}
		next(w, r)
	}
}

func printData(data *Data) {
	fmt.Printf("{ \n"+
		" WebsiteUrl: %s, \n"+
		" SessionId: %s \n"+
		" ResizeFrom: %s \n"+
		" ResizeTo: %s \n"+
		" CopyAndPaste: %v \n"+
		" FormCompletionTime: %d \n"+
		"}\n", data.WebsiteUrl, data.SessionId, data.ResizeFrom, data.ResizeTo, data.CopyAndPaste, data.FormCompletionTime)
}
