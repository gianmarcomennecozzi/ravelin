package server

import (
	"bytes"
	"fmt"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"os"
	"sync"
	"testing"
	"time"
)

const ConcurrentReq = 100

func TestConcurrentClients(t *testing.T) {
	os.Stdout = nil
	newDir, err := os.Getwd()
	if err != nil {
		t.Fatalf("error getting working directory: %s", err)
	}
	//set the working directory to let execute the template in the handleIndexPage
	os.Chdir(newDir[:len(newDir)-len("server")])

	s := NewServer()
	srv := httptest.NewServer(s.Handler())

	var wg sync.WaitGroup
	resps := make(chan error)

	for i := 0; i < ConcurrentReq; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			req, err := http.NewRequest(http.MethodPost, srv.URL+"/", nil)
			if err != nil {
				resps <- err
				return
			}

			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				resps <- err
				return
			}
			defer resp.Body.Close()

			if resp.StatusCode != http.StatusOK {
				resps <- fmt.Errorf("error status code, expected 200, got %d", resp.StatusCode)
			}
		}()
	}

	wg.Wait()

	if len(resps) > 0 {
		t.Fatalf("Error: %v", <-resps)
	}
	close(resps)
}

func TestGetDataFromRequest(t *testing.T) {
	os.Stdout = nil
	s := NewServer()
	goodCookievalue := "whatever"
	s.data[goodCookievalue] = &Data{}

	tl := []struct {
		name        string
		cookieValue string
		err         bool
	}{
		{name: "Normal Case", cookieValue: goodCookievalue, err: false},
		{name: "Bad Case", cookieValue: "thisisatest", err: true},
	}

	for _, tc := range tl {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest(http.MethodPost, "/", nil)
			if err != nil {
				t.Fatal(err)
			}

			req.AddCookie(&http.Cookie{
				Name:  SESSION,
				Value: tc.cookieValue,
				Path:  "/",
			})

			_, err = s.getDataFromRequest(req)
			if err != nil {
				if !tc.err {
					t.Fatal(err)
				}
			}

			if err == nil && tc.err {
				t.Fatal("Expected error")
			}
		})
	}
}

func TestHandlerFunctions(t *testing.T) {
	os.Stdout = nil
	tl := []struct {
		name     string
		pathReq  string
		bodyReq  []byte
		respCode int
	}{
		{name: "Resize Good", pathReq: "/resize", bodyReq: []byte(`{"from_width":"100", "from_height":"100", "to_width":"50", "to_height":"50"}`), respCode: http.StatusOK},
		{name: "Resize Bad", pathReq: "/resize", bodyReq: []byte(`{"from_width":"100", "from_height":100}`), respCode: http.StatusInternalServerError},
		{name: "CopyPaste Good 1", pathReq: "/copy-paste", bodyReq: []byte(`{"field":"whatever"}`), respCode: http.StatusOK},
		{name: "CopyPaste Good 2", pathReq: "/copy-paste", bodyReq: []byte(`{"field":"whatever-part2"}`), respCode: http.StatusOK},
		{name: "Time Taken Good", pathReq: "/time-taken", bodyReq: []byte(`{"time": 100}`), respCode: http.StatusOK},
		{name: "Time Taken Bad", pathReq: "/time-taken", bodyReq: []byte(`{"time": "100"}`), respCode: http.StatusInternalServerError},
	}

	s := NewServer()
	srv := httptest.NewServer(s.Handler())

	//Create and map Data inside the server
	req, err := http.NewRequest(http.MethodPost, "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	sessionsID := s.createSessionID(req)

	for _, tc := range tl {
		t.Run(tc.name, func(t *testing.T) {
			newReq, err := http.NewRequest(http.MethodPost, srv.URL+tc.pathReq, bytes.NewBuffer(tc.bodyReq))
			if err != nil {
				t.Fatal(err)
			}

			newReq.AddCookie(&http.Cookie{
				Name:  SESSION,
				Value: sessionsID,
				Path:  "/",
			})

			resp, err := http.DefaultClient.Do(newReq)
			if err != nil {
				t.Fatal(err)
			}
			defer resp.Body.Close()

			if resp.StatusCode != tc.respCode {
				t.Fatalf("Expected %d, got %d", tc.respCode, resp.StatusCode)
			}
		})
	}
}

func TestMultipleClientSameID(t *testing.T) {
	os.Stdout = nil
	s := NewServer()
	srv := httptest.NewServer(s.Handler())

	//Create and map Data inside the server
	req, err := http.NewRequest(http.MethodPost, "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	sessionsID := s.createSessionID(req)

	var wg sync.WaitGroup
	resps := make(chan error)

	paths := []string{"/copy-paste", "/time-taken", "/resize"}
	bodies := [][]byte{
		[]byte(`{"field":"whatever"}`),
		[]byte(`{"time": 100}`),
		[]byte(`{"from_width":"100", "from_height":"100", "to_width":"50", "to_height":"50"}`),
	}

	for i := 0; i < ConcurrentReq; i++ {
		wg.Add(1)

		//random select an element to pass in the go routines
		rand.Seed(time.Now().UnixNano())
		randI := rand.Intn(len(paths))

		go func(path string, body []byte) {
			defer wg.Done()

			newReq, err := http.NewRequest(http.MethodPost, srv.URL+path, bytes.NewBuffer(body))
			if err != nil {
				resps <- err
				return
			}
			newReq.AddCookie(&http.Cookie{
				Name:  SESSION,
				Value: sessionsID,
				Path:  "/",
			})

			resp, err := http.DefaultClient.Do(newReq)
			if err != nil {
				resps <- err
				return
			}
			defer resp.Body.Close()

			if resp.StatusCode != http.StatusOK {
				resps <- fmt.Errorf("error status code, expected 200, got %d", resp.StatusCode)
			}

		}(paths[randI], bodies[randI])
	}

	wg.Wait()

	if len(resps) > 0 {
		t.Fatalf("Error: %v", <-resps)
	}
	close(resps)

}

func TestConcurrentCopyPaste(t *testing.T) {
	os.Stdout = nil
	s := NewServer()
	srv := httptest.NewServer(s.Handler())

	//Create and map Data inside the server
	req, err := http.NewRequest(http.MethodPost, "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	sessionsID := s.createSessionID(req)

	var wg sync.WaitGroup
	resps := make(chan error)

	for i := 0; i < ConcurrentReq; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			//random select an element to pass in the request below
			l := []string{"field1", "field20", "test30", "field40", "field9"}
			rand.Seed(time.Now().UnixNano())
			randI := rand.Intn(len(l))

			newReq, err := http.NewRequest(http.MethodPost, srv.URL+"/copy-paste", bytes.NewBuffer([]byte(fmt.Sprintf(`{"field":"%s"}`, l[randI]))))
			if err != nil {
				resps <- err
				return
			}
			newReq.AddCookie(&http.Cookie{
				Name:  SESSION,
				Value: sessionsID,
				Path:  "/",
			})

			resp, err := http.DefaultClient.Do(newReq)
			if err != nil {
				resps <- err
				return
			}
			defer resp.Body.Close()

			if resp.StatusCode != http.StatusOK {
				resps <- fmt.Errorf("error status code, expected 200, got %d", resp.StatusCode)
			}
		}()
	}
	wg.Wait()

	if len(resps) > 0 {
		t.Fatalf("Error: %v", <-resps)
	}
	close(resps)
}
