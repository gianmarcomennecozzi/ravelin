# Ravelin code test 

Created a server that accepts JSON data via HTTP POST requests.
A cookie will be assigned to each client that makes a request to the index page. 
The cookie will be the unique session id for the client.

The POST requests handled by the server are described in [1].
The server will be responsible to get the session ID from the cookie, and assign the JSON data to the correct `Data` struct.

### Usage

Clone the repository and either run the code through docker
```bash
$ git clone https://github.com/gianmarcomennecozzi/code-test-ravelin.git
$ cd code-test-ravelin
$ docker build -t ravelin .
$ docker run -p 8080:8080 ravelin
```
or in local
```bash
$ git clone https://github.com/gianmarcomennecozzi/code-test-ravelin.git
$ cd code-test-ravelin
$ go mod download
$ go run main.go
```

Browse ```localhost:8080```

### Test 
```bash
go test -v --race ./...
```

### Reference

[1] https://github.com/unravelin/code-test 